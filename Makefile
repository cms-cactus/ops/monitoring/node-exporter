SHELL:=/bin/bash
.DEFAULT_GOAL := help

#
# main variables
#

PROJECT_NAME=node-exporter
VERSION ?= $(shell git describe --always)
NODE_EXPORTER_VERSION ?= 1.6.1
ARCH=amd64
RPM_NAME = cactus-node-exporter-${VERSION}-${RELEASE}.${ARCH}.rpm

#
# main targets
#

.PHONY: rpm
rpm: ${RPM_NAME} ## Builds RPM package

.PHONY: clean
clean: ## Cleans up project
	rm -rf rpms ${NODE_EXPORTER_STR} rpmroot

#
# dependencies
#

NODE_EXPORTER_STR = node_exporter-${NODE_EXPORTER_VERSION}.linux-${ARCH}
${NODE_EXPORTER_STR}:
	curl -LO https://github.com/prometheus/node_exporter/releases/download/v${NODE_EXPORTER_VERSION}/${NODE_EXPORTER_STR}.tar.gz
	tar xf ${NODE_EXPORTER_STR}.tar.gz
	rm -rf ${NODE_EXPORTER_STR}.tar.gz
	touch  ${NODE_EXPORTER_STR}
	

${RPM_NAME}: ${NODE_EXPORTER_STR} *
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus/bin/node-exporter rpmroot/usr/lib/systemd/system rpmroot/opt/cactus/etc/default/ rpmroot/opt/cactus/etc/node-exporter/
	cp systemd/* rpmroot/usr/lib/systemd/system
	cp ${NODE_EXPORTER_STR}/node_exporter rpmroot/opt/cactus/bin/node-exporter
	cp default/cactus-node-exporter.default rpmroot/opt/cactus/etc/default/cactus-node-exporter

	mkdir -p rpms
	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-${PROJECT_NAME} \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--vendor CERN \
	--description "Node exporter for the L1 Online Software at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/node-exporter" \
	--provides cactus-${PROJECT_NAME} \
	.=/ && mv *.rpm ../rpms/

.PHONY: help
help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
